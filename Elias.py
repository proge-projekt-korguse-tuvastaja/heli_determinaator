import sounddevice as sd
import numpy as np
import scipy.fftpack
import os
import time
import matplotlib.pyplot as plt


F0 = 440
noodid = ["A","A#","H","C","C#","D","D#","E","F","F#","G","G#"]

sample_freq = 44100 #mitu mõõtmist toimub sekundis sõltub mikrofonist
salvestuse_pikkus = 1 #sek
salvestuse_pikkus2 = sample_freq * salvestuse_pikkus #samplites
sample_kestus = 1/sample_freq
ANDMED = [0 for i in range(salvestuse_pikkus2)]

def leia_lähim_noot(Fx):
    x = 12*(np.log2(Fx/F0))
    x = int(x.round())
    lähim_noot = noodid[x%12]
    noodi_oktav = ((x + 9)//12)+4
    lähim_Fx = 440*2**(x/12)
    return lähim_noot, noodi_oktav, lähim_Fx

def leia_sagedus(salvestus, sample_freq, salvestuse_pikkus2):
    ANDMED = salvestus  ##### TÕENÄOLISELT ON VIGA SIIN
    print(ANDMED, len(ANDMED))  ##### TÕENÄOLISELT ON VIGA SIIN
    magnituud = abs(scipy.fftpack.fft(ANDMED)[:len(ANDMED)])
    
    for i in range(int(62/(sample_freq/salvestuse_pikkus2))):
      magnituud[i] = 0  #vähendab müra
    
    max_väärtus = np.argmax(magnituud)
    sagedus = max_väärtus * (sample_freq/salvestuse_pikkus2)
    return sagedus


while True:
    salvestus = sd.rec(salvestuse_pikkus * sample_freq, samplerate=sample_freq, channels=1,dtype='float64')
    sd.wait()
    s =leia_sagedus(salvestus, sample_freq, salvestuse_pikkus2)
    print(leia_lähim_noot(s))


    
    
    
