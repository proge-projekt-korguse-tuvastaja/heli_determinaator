#Kas nõnda seda visuaalset klaverit saaks teha
# Siin nii palju, kui palju mul praegu on, kuvab lihtsalt c-c klahve

import pygame
import sys
from tkinter import *

pygame.init()

def puhas():
    txtDisplay.delete(0, END)
    num1.set(0)
    return

def value_C():
    num1.set("C")
    sound = pygame.mixer.Sound("frequency")
    sound.play()
    return

def value_D():
    num1.set("D")
    sound = pygame.mixer.Sound("Frequency")
    sound.play()
    return

def value_E():
    num1.set("E")
    sound = pygame.mixer.Sound("frequency")
    sound.play()
    return

def value_F():
    num1.set("F")
    sound = pygame.mixer.Sound("Frequency")
    sound.play()
    return

def value_G():
    num1.set("G")
    sound = pygame.mixer.Sound("frequency")
    sound.play()
    return

def value_A():
    num1.set("A")
    sound = pygame.mixer.Sound("Frequency")
    sound.play()
    return

def value_H():
    num1.set("H")
    sound = pygame.mixer.Sound("frequency")
    sound.play()
    return

def value_Cs():
    num1.set("C#")
    sound = pygame.mixer.Sound("Frequency")
    sound.play()
    return

def value_Ds():
    num1.set("D#")
    sound = pygame.mixer.Sound("frequency")
    sound.play()
    return

def value_Es():
    num1.set("E#")
    sound = pygame.mixer.Sound("Frequency")
    sound.play()
    return

def value_Fs():
    num1.set("F#")
    sound = pygame.mixer.Sound("frequency")
    sound.play()
    return

def value_Gs():
    num1.set("G")
    sound = pygame.mixer.Sound("Frequency")
    sound.play()
    return

def value_As():
    num1.set("A#")
    sound = pygame.mixer.Sound("frequency")
    sound.play()
    return
def value_Hs():
    num1.set("H#")
    sound = pygame.mixer.Sound("Frequency")
    sound.play()
    return

root = Tk()
frame = Frame(root)
frame.pack()

root.title("Piano")

num1 = StringVar()
num1.set(0)
num2 = StringVar()
operator=StringVar()

topframe = Frame(root)
topframe.pack(side = TOP)
txtDisplay = Entry(frame, textvariable = num1, bd = 20, insertwidth = 1, font = 30, justify = 'right', width = 4)
txtDisplay.pack(side = TOP)


button1 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "C", bg = "black", fg = "white")
button1.pack(side = LEFT)

button2 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "D", bg = "black", fg = "white")
button2.pack(side = LEFT)

button3 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "E", bg = "black", fg = "white")
button3.pack(side = LEFT)

button4 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "F", bg = "black", fg = "white")
button4.pack(side = LEFT)

button5 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "G", bg = "black", fg = "white")
button5.pack(side = LEFT)

button6 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "A", bg = "black", fg = "white")
button6.pack(side = LEFT)

button7 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "H", bg = "black", fg = "white")
button7.pack(side = LEFT)

button8 = Button(topframe, padx = 8, height = 6, pady = 8, bd = 8, text = "C", bg = "black", fg = "white")
button8.pack(side = LEFT)



root.mainloop()